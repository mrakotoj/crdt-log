package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"

	"github.com/gorilla/mux"
)

func main() {

	go replicate()

	router := mux.NewRouter()
	router.HandleFunc("/", addLog).Methods("POST")
	router.HandleFunc("/", getLog).Methods("GET")

	log.Fatal(http.ListenAndServe(":8080", router))
}

func getLog(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	sites := r.Form["sites"]
	sort.Strings(sites)

	c, err := getDocsForSites(sites)
	if err != nil {
		log.Println(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	sortDocuments(c.Docs)

	for _, doc := range c.Docs {
		fmt.Fprintf(w, "%s\n", doc.Payload)
	}
}

func addLog(w http.ResponseWriter, r *http.Request) {
	newEntry, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)
		return
	}

	r.ParseForm()
	sites := r.Form["sites"]
	sort.Strings(sites)

	c, err := getDocsForSites(sites)
	if err != nil {
		log.Println(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	max := uint64(0)
	for _, d := range c.Docs {
		if d.Generation >= max {
			max = d.Generation
		}
	}

	newDoc := Document{
		Locations:  sites,
		Generation: max + 1,
		Payload:    string(newEntry),
	}

	buf, err := json.Marshal(newDoc)
	if err != nil {
		log.Println(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	newresp, err := http.Post("http://localhost:5984/crdt-log", "application/json", bytes.NewReader(buf))

	if err != nil {
		log.Println(err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(newresp.StatusCode)
}

func getDocsForSites(sites []string) (*CouchResp, error) {
	locations := make([]string, 0)
	for _, site := range sites {
		locations = append(locations, fmt.Sprintf(`{"Locations": {"$all": ["%s"]}}`, site))
	}
	selector := fmt.Sprintf(`{"selector": {"$and": [%s]}}`, strings.Join(locations, ","))

	fmt.Println(selector)
	current, err := http.Post("http://localhost:5984/crdt-log/_find", "application/json", strings.NewReader(selector))
	if err != nil {
		return nil, err
	}
	if current.StatusCode != 200 {
		return nil, fmt.Errorf("Post %s: %s", current.Request.URL.String(), current.Status)
	}

	var c CouchResp

	err = json.NewDecoder(current.Body).Decode(&c)
	current.Body.Close()
	if err != nil {
		return nil, err
	}

	return &c, nil
}

type CouchResp struct {
	Docs []Document `json:"docs"`
}
