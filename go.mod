module gitlab.inria.fr/discovery/crdt-log

go 1.19

require (
	github.com/gorilla/mux v1.8.0
	github.com/neurodrone/crdt v0.0.0-20160128033235-68acdd766950
)

require (
	github.com/benbjohnson/clock v1.3.5 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
